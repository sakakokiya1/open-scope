package com.mofum.scope.spring.web.constant;

/**
 * 权限范围配置
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-26
 **/
public class ScopeConfig {

    public static final ScopeConfig CONFIG = new ScopeConfig();

    private String serviceColumns = Constant.SCOPE_SERVICE_COLUMN;

    private ScopeConfig() {
    }

    public String getServiceColumns() {
        return serviceColumns;
    }

    public void setServiceColumns(String serviceColumns) {
        this.serviceColumns = serviceColumns;
    }

    public void safeSetServiceColumns(String serviceColumns) {
        synchronized (this) {
            this.serviceColumns = serviceColumns;
        }
    }
}
