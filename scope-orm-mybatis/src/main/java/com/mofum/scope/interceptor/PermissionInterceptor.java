package com.mofum.scope.interceptor;

import com.mofum.scope.common.model.Permission;
import com.mofum.scope.common.utils.ObjectUtils;
import com.mofum.scope.config.ScopeConfig;
import com.mofum.scope.processor.IRestructureProcessor;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import com.mofum.scope.processor.impl.DefaultRestructureProcessor;

import java.util.Properties;

/**
 * 权限拦截器
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-19
 **/
@Intercepts({@Signature(
        type = Executor.class,
        method = "query",
        args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}
), @Signature(
        type = Executor.class,
        method = "query",
        args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class}
)})
public class PermissionInterceptor implements Interceptor {

    /**
     * 重构处理器
     */
    private IRestructureProcessor restructureProcessor;

    public Object intercept(Invocation invocation) throws Throwable {
        Object[] args = invocation.getArgs();
        MappedStatement ms = (MappedStatement) args[0];
        Object parameter = args[1];
        RowBounds rowBounds = (RowBounds) args[2];
        ResultHandler resultHandler = (ResultHandler) args[3];
        Executor executor = (Executor) invocation.getTarget();
        CacheKey cacheKey;
        BoundSql boundSql;
        if (args.length == 4) {
            boundSql = ms.getBoundSql(parameter);
            cacheKey = executor.createCacheKey(ms, parameter, rowBounds, boundSql);
        } else {
            cacheKey = (CacheKey) args[4];
            boundSql = (BoundSql) args[5];
        }

        Permission permission = ObjectUtils.toObject(parameter, Permission.class);

        if (permission != null) {

            if (ScopeConfig.ENABLE_RESTRUCTURE) {
                boundSql = getRestructureProcessor().process(invocation, boundSql, permission);
            }

        }

        return executor.query(ms, parameter, rowBounds, resultHandler, cacheKey, boundSql);
    }


    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    public void setProperties(Properties properties) {

    }

    public IRestructureProcessor getRestructureProcessor() {
        if (restructureProcessor == null) {
            restructureProcessor = new DefaultRestructureProcessor();
        }
        return restructureProcessor;
    }

    public void setRestructureProcessor(IRestructureProcessor restructureProcessor) {
        this.restructureProcessor = restructureProcessor;
    }
}
