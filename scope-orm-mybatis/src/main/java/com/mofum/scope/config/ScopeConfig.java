package com.mofum.scope.config;

/**
 * 范围配置
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-19
 **/
public class ScopeConfig {

    /**
     * 是否提取LIMIT 到外部SQL
     */
    public static boolean STATUS_EXTRACT_LIMIT = true;

    /**
     * 是否开启重构SQL
     */
    public static boolean ENABLE_RESTRUCTURE = true;

    public static final String DEFAULT_SCOPE_ALIAS = "SCOPE_MOFUM_ALIAS";

    public static String OUTER_TABLE_ALIAS = DEFAULT_SCOPE_ALIAS;
}
