package com.mofum.scope.processor;

import com.mofum.scope.common.model.Permission;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.plugin.Invocation;

/**
 * @author yuyang@qxy37.com
 * @since 2019-03-19
 **/
public interface IRestructureProcessor {

    /**
     * 处理SQL
     *
     * @param invocation    调用
     * @param permissionSql 权限SQL
     * @param permission    权限实体
     * @return
     */
    BoundSql process(Invocation invocation, BoundSql permissionSql, Permission permission);

}