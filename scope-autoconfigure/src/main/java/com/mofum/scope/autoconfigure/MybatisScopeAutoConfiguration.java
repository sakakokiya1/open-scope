package com.mofum.scope.autoconfigure;

import com.mofum.scope.interceptor.PermissionInterceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.Iterator;
import java.util.List;

/**
 * Mybatis 权限范围自动配置类
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-20
 **/
@Configuration
@ConditionalOnBean({SqlSessionFactory.class})
@EnableConfigurationProperties({AutoMybatisScopeProperties.class})
@ConditionalOnProperty//存在对应配置信息时初始化该配置类
        (
                prefix = "mofum.mybatis.scope",//存在配置前缀hello
                name = "needMybatis",
                matchIfMissing = true,//缺失检查
                havingValue = "true"
        )
public class MybatisScopeAutoConfiguration {

    @Autowired
    private List<SqlSessionFactory> sqlSessionFactoryList;

    public MybatisScopeAutoConfiguration() {
    }

    @PostConstruct
    public void addTestInterceptor() {
        PermissionInterceptor interceptor = new PermissionInterceptor();

        Iterator var3 = this.sqlSessionFactoryList.iterator();

        while (var3.hasNext()) {
            SqlSessionFactory sqlSessionFactory = (SqlSessionFactory) var3.next();
            sqlSessionFactory.getConfiguration().addInterceptor(interceptor);
        }

    }

}
