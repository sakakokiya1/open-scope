package com.mofum.scope.annotation.controller;

import com.mofum.scope.common.IScopeAuthenticator;
import com.mofum.scope.common.IScopeConverter;
import com.mofum.scope.common.IScopeExtractor;

import java.lang.annotation.*;

/**
 * 查询范围注解
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-20
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface QueryScope {

    /**
     * 范围提取器
     *
     * @return
     */
    Class<? extends IScopeExtractor>[] extractors() default {};

    /**
     * 范围认证器
     *
     * @return
     */
    Class<? extends IScopeAuthenticator>[] authenticators() default {};

    /**
     * 业务ID 转换范围ID
     *
     * @return
     */
    Class<? extends IScopeConverter>[] converts() default {};

    /**
     * 默认打开范围提取器
     *
     * @return
     */
    boolean enableExtractor() default true;

    /**
     * 默认关闭范围认证器
     *
     * @return
     */
    boolean enableAuthenticator() default false;

    /**
     * 默认关闭转换器
     *
     * @return
     */
    boolean enableConverter() default false;

    /**
     * 业务列
     *
     * @return
     */
    String[] serviceColumns() default {};

    /**
     * 默认关闭自定义列。（因为使用自定义列，需要额外处理Args参数）
     *
     * @return
     */
    boolean enableCustomServiceColumns() default false;

    /**
     * 范围类型
     * @return
     */
    String type() default "";

    /**
     * 业务列
     *
     * @return
     */
    ServiceColumn[] columns() default {};

}
