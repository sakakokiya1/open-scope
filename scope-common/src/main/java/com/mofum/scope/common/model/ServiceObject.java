package com.mofum.scope.common.model;

/**
 * @author yuyang@qxy37.com
 * @since 2019-03-26
 **/
public class ServiceObject {

    private String type;

    private Object data;

    public ServiceObject() {
    }

    public ServiceObject(String type, Object data) {
        this.type = type;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
