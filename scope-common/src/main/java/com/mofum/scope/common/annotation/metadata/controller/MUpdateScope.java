package com.mofum.scope.common.annotation.metadata.controller;

import com.mofum.scope.common.IScopeAuthenticator;
import com.mofum.scope.common.IScopeConverter;
import com.mofum.scope.common.IScopeExtractor;

/**
 * 增删改的范围
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-20
 **/
public class MUpdateScope {

    /**
     * 数据范围提取器
     */
    private Class<? extends IScopeExtractor>[] extractors;

    /**
     * 数据范围验证器
     */
    private Class<? extends IScopeAuthenticator>[] authenticators;

    /**
     * 业务ID 转范围ID
     *
     * @return
     */
    private Class<? extends IScopeConverter>[] converters;

    /**
     * 开启提取
     */
    private boolean enableExtractor;

    /**
     * 开启认证
     */
    private boolean enableAuthenticator;

    /**
     * 开启转换器
     */
    private boolean enableConverter;

    /**
     * 业务列
     */
    private String[] serviceColumns;

    /**
     * 开启自定义业务字段列，则业务列失效
     */
    private boolean enableCustomServiceColumns;

    /**
     * 范围类型
     */
    private String type;

    /**
     * 业务列
     */
    private MServiceColumn[] columns;

    public Class<? extends IScopeExtractor>[] getExtractors() {
        return extractors;
    }

    public void setExtractors(Class<? extends IScopeExtractor>[] extractors) {
        this.extractors = extractors;
    }

    public Class<? extends IScopeAuthenticator>[] getAuthenticators() {
        return authenticators;
    }

    public void setAuthenticators(Class<? extends IScopeAuthenticator>[] authenticators) {
        this.authenticators = authenticators;
    }

    public boolean isEnableExtractor() {
        return enableExtractor;
    }

    public void setEnableExtractor(boolean enableExtractor) {
        this.enableExtractor = enableExtractor;
    }

    public boolean isEnableAuthenticator() {
        return enableAuthenticator;
    }

    public void setEnableAuthenticator(boolean enableAuthenticator) {
        this.enableAuthenticator = enableAuthenticator;
    }

    public Class<? extends IScopeConverter>[] getConverters() {
        return converters;
    }

    public void setConverters(Class<? extends IScopeConverter>[] converters) {
        this.converters = converters;
    }

    public boolean isEnableConverter() {
        return enableConverter;
    }

    public void setEnableConverter(boolean enableConverter) {
        this.enableConverter = enableConverter;
    }

    public String[] getServiceColumns() {
        return serviceColumns;
    }

    public void setServiceColumns(String[] serviceColumns) {
        this.serviceColumns = serviceColumns;
    }

    public boolean isEnableCustomServiceColumns() {
        return enableCustomServiceColumns;
    }

    public void setEnableCustomServiceColumns(boolean enableCustomServiceColumns) {
        this.enableCustomServiceColumns = enableCustomServiceColumns;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public MServiceColumn[] getColumns() {
        return columns;
    }

    public void setColumns(MServiceColumn[] columns) {
        this.columns = columns;
    }
}
