package com.mofum.scope.common;

import com.mofum.scope.common.model.Scope;

import java.util.List;

/**
 * @author yuyang@qxy37.com
 * @since 2019-03-25
 **/
public interface IScopeConverter<Params extends Object, ServiceException extends Exception> {

    /**
     * 转换业务参数为范围参数
     *
     * @param params
     * @return
     * @throws ServiceException
     */
    List<Scope> convert2Scope(Params params) throws ServiceException;

}