package com.mofum.scope.common.model;

/**
 * 范围
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-20
 **/
public class Scope {

    /**
     * 范围ID
     */
    private String id;

    /**
     * 范围类型
     */
    private String type;

    /**
     * 范围列
     */
    private String column;

    /**
     * 表名
     */
    private String tableName;

    /**
     * 原始数据
     */
    private Object origData;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Object getOrigData() {
        return origData;
    }

    public void setOrigData(Object origData) {
        this.origData = origData;
    }

    @Override
    public String toString() {
        return "Scope{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", column='" + column + '\'' +
                ", tableName='" + tableName + '\'' +
                ", origData=" + origData +
                '}';
    }
}
