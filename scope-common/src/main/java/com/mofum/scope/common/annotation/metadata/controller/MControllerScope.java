package com.mofum.scope.common.annotation.metadata.controller;

import com.mofum.scope.common.IScopeAuthenticator;
import com.mofum.scope.common.IScopeConverter;
import com.mofum.scope.common.IScopeExtractor;

/**
 * 控制器范围
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-21
 **/
public class MControllerScope {

    /**
     * 忽略的方法名
     *
     * @return
     */
    private String[] ignoreMethods;

    /**
     * 查询的方法名
     *
     * @return
     */
    private String[] queryMethods;

    /**
     * 更新的方法名
     *
     * @return
     */
    private String[] updateMethods;

    /**
     * 范围提取器
     *
     * @return
     */
    private Class<? extends IScopeExtractor>[] extractors;

    /**
     * 范围认证器
     *
     * @return
     */
    private Class<? extends IScopeAuthenticator>[] authenticators;

    /**
     * 业务ID 转范围ID
     *
     * @return
     */
    private Class<? extends IScopeConverter>[] converters;


    /**
     * 默认打开范围提取器
     *
     * @return
     */
    private boolean enableExtractor;

    /**
     * 默认关闭范围认证器
     *
     * @return
     */
    private boolean enableAuthenticator;

    /**
     * 开启转换器
     */
    private boolean enableConverter;

    /**
     * 业务列
     */
    private String[] serviceColumns;

    /**
     * 开启自定义业务字段列，则业务列失效
     */
    private boolean enableCustomServiceColumns;

    /**
     * 范围类型
     */
    private String type;

    /**
     * 业务列
     */
    private MServiceColumn[] columns;

    public String[] getIgnoreMethods() {
        return ignoreMethods;
    }

    public void setIgnoreMethods(String[] ignoreMethods) {
        this.ignoreMethods = ignoreMethods;
    }

    public String[] getQueryMethods() {
        return queryMethods;
    }

    public void setQueryMethods(String[] queryMethods) {
        this.queryMethods = queryMethods;
    }

    public String[] getUpdateMethods() {
        return updateMethods;
    }

    public void setUpdateMethods(String[] updateMethods) {
        this.updateMethods = updateMethods;
    }

    public Class<? extends IScopeExtractor>[] getExtractors() {
        return extractors;
    }

    public void setExtractors(Class<? extends IScopeExtractor>[] extractors) {
        this.extractors = extractors;
    }

    public Class<? extends IScopeAuthenticator>[] getAuthenticators() {
        return authenticators;
    }

    public void setAuthenticators(Class<? extends IScopeAuthenticator>[] authenticators) {
        this.authenticators = authenticators;
    }

    public boolean isEnableExtractor() {
        return enableExtractor;
    }

    public void setEnableExtractor(boolean enableExtractor) {
        this.enableExtractor = enableExtractor;
    }

    public boolean isEnableAuthenticator() {
        return enableAuthenticator;
    }

    public void setEnableAuthenticator(boolean enableAuthenticator) {
        this.enableAuthenticator = enableAuthenticator;
    }

    public Class<? extends IScopeConverter>[] getConverters() {
        return converters;
    }

    public void setConverters(Class<? extends IScopeConverter>[] converters) {
        this.converters = converters;
    }

    public boolean isEnableConverter() {
        return enableConverter;
    }

    public void setEnableConverter(boolean enableConverter) {
        this.enableConverter = enableConverter;
    }

    public String[] getServiceColumns() {
        return serviceColumns;
    }

    public void setServiceColumns(String[] serviceColumns) {
        this.serviceColumns = serviceColumns;
    }

    public boolean isEnableCustomServiceColumns() {
        return enableCustomServiceColumns;
    }

    public void setEnableCustomServiceColumns(boolean enableCustomServiceColumns) {
        this.enableCustomServiceColumns = enableCustomServiceColumns;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public MServiceColumn[] getColumns() {
        return columns;
    }

    public void setColumns(MServiceColumn[] columns) {
        this.columns = columns;
    }
}
