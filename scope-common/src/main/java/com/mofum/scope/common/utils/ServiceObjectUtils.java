package com.mofum.scope.common.utils;

import com.mofum.scope.common.model.Scope;
import com.mofum.scope.common.model.ServiceObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yuyang@qxy37.com
 * @since 2019-03-27
 **/
public class ServiceObjectUtils {

    public static Map<String, List<ServiceObject>> convert2TypeMap(List<Object> list) {

        if (list == null) {
            return null;
        }

        Map<String, List<ServiceObject>> map = new HashMap<>();

        for (Object object : list) {

            if (object instanceof ServiceObject) {

                ServiceObject serviceObject = (ServiceObject) object;

                List<ServiceObject> dataList = map.get(serviceObject.getType());

                if (dataList == null) {
                    dataList = new ArrayList<>();
                    map.put(serviceObject.getType(), dataList);
                }

                dataList.add(serviceObject);

            }

        }
        return map;

    }

    public static Map<String, List<String>> convert2StringList(List<Object> list) {
        return convert2StringList(list, false);
    }

    public static Map<String, List<String>> convert2StringList(List<Object> list, boolean includeObjectClass) {

        Map<String, List<String>> serviceObjects = null;

        if (list != null) {

            serviceObjects = new HashMap<>();

            for (Object object : list) {

                if (object instanceof ServiceObject) {

                    ServiceObject serviceObject = (ServiceObject) object;

                    List<String> dataList = serviceObjects.get(serviceObject.getType());

                    if (dataList == null) {
                        dataList = new ArrayList<>();
                        serviceObjects.put(serviceObject.getType(), dataList);
                    }

                    Object data = serviceObject.getData();
                    if (data != null) {

                        if (String.class.equals(data.getClass())) {
                            dataList.add((String) data);
                        } else {
                            if (includeObjectClass) {
                                dataList.add(data.toString());
                            }
                        }

                    }


                }

            }

        }


        return serviceObjects;

    }

    public static Map<String, List<Scope>> convert2ScopeList(List<Object> list) {
        return convert2ScopeList(list, false);
    }

    public static Map<String, List<Scope>> convert2ScopeList(List<Object> list, boolean includeObjectClass) {

        Map<String, List<Scope>> serviceObjects = null;

        if (list != null) {

            serviceObjects = new HashMap<>();

            for (Object object : list) {

                if (object instanceof ServiceObject) {

                    ServiceObject serviceObject = (ServiceObject) object;

                    List<Scope> dataList = serviceObjects.get(serviceObject.getType());

                    if (dataList == null) {
                        dataList = new ArrayList<>();
                        serviceObjects.put(serviceObject.getType(), dataList);
                    }

                    Object data = serviceObject.getData();
                    if (data != null) {

                        if (String.class.equals(data.getClass())) {
                            Scope scope = new Scope();
                            scope.setId((String) data);
                        } else {
                            if (includeObjectClass) {
                                Scope scope = new Scope();
                                scope.setId(data.toString());
                            }
                        }

                    }


                }

            }

        }


        return serviceObjects;

    }


}
